//
//  ViewController.swift
//  Test app 2
//
//  Created by Илья on 12/02/2019.
//  Copyright © 2019 IG. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    var fetchedUser = [allUsers] ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        parseData()
        tableView.tableFooterView = UIView()
      
    }
    
    func parseData () {
        
        fetchedUser = []
        
        let url = "https://api.github.com/users?since=0"
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
        
        let task = session.dataTask(with: request) { (data, resonse, error) in
            
            if (error != nil) {
                print("Error")
            } else {
                
                do {
                    let fetchedData = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSArray
                    
                    for eachFetchUser in fetchedData {
                        let eachuser = eachFetchUser as! [String : Any]
                        let login = eachuser["login"] as! String
                        let id = eachuser["id"] as! Int
                       let conssst = String(id)
                        let avatarUrl = eachuser["avatar_url"] as! String
                        
                        self.fetchedUser.append(allUsers(login: login,id: conssst, avatar_url: avatarUrl))
                     DispatchQueue.main.async {
                             self.tableView.reloadData()
                      }
                    
                    }
                  
                    
                } catch {
                    print("Error 2")
                }
            }
        }
        task.resume()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ArticleCell
        
        cell.title.text = fetchedUser[indexPath.row].login
        cell.desc.text = fetchedUser[indexPath.row].id
        
        
      if let imageURL = URL(string: fetchedUser[indexPath.row].avatar_url) {
      
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: imageURL)
            if let data = data {
                let image = UIImage(data: data)
                DispatchQueue.main.async {
                    cell.imgView.image = image
                }
            }
        }
        }
        
        return cell
    }
    
    }




