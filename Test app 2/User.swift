//
//  Article.swift
//  newsReader
//
//  Created by Vasil Nunev on 30/10/2016.
//  Copyright © 2016 Nunev. All rights reserved.
//

import UIKit

class allUsers {
    var login: String
    var id: String
    var avatar_url: String
    
    init(login: String, id: String, avatar_url: String) {
        self.login = login
        self.id = id
        self.avatar_url = avatar_url
    }
}
